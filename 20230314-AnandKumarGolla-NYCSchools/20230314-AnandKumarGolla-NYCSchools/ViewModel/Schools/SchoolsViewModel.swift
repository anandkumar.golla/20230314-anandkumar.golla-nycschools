//
//  SchoolsViewModel.swift
//  20230314-AnandKumarGolla-NYCSchools
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import Foundation

class SchoolsViewModel {
    
    weak var schoolsService: SchoolsServiceProtocol?
    
    var schools: [School] = []
    var searchedSchools = [School]()
    var searching = false
    
    var onErrorHandling : ((ErrorResult?) -> Void)?
    var onFetchSchools : ((Bool) -> Void)?
    
    
    init(schoolsViewService: SchoolsServiceProtocol?) {
        self.schoolsService = schoolsViewService
    }
    
    /// Method to fetch list of schools
    func fetchSchools() {
        
        guard let service = schoolsService else {
            onErrorHandling?(ErrorResult.custom(string: "Missing service"))
            return
        }
        
        service.fetchSchools { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let schools) :
                    let sortedSchools = schools.sorted(by: { $0.school_name < $1.school_name })
                    self.schools = sortedSchools
                    self.onFetchSchools?(true)
                case .failure(let error) :
                    self.onErrorHandling?(error)
                }
            }
        }
    }
}
