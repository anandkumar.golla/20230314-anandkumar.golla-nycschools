//
//  SchoolDetailViewModel.swift
//  20230314-AnandKumarGolla-NYCSchools
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import Foundation
import UIKit

internal final class SchoolDetailViewModel: ObservableObject {
    
    @Published internal var showingAlert = false
    @Published internal var errorString = "" {
        didSet {
            showingAlert = true
        }
    }
    @Published internal var satScore: SatScore?
    
    weak var service: SATScoresServiceProtocol?

    init(service: SATScoresServiceProtocol?) {
        self.service = service
    }
    
    /// Method to fetch SatScore from the server
    /// - Parameter dbn: dbn of the school
    func fetchSatScore(dbn: String) {
        service?.fetchSatScores(dbn) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let satScore) :
                    self.satScore = satScore
                case .failure(let error) :
                    self.errorString = error.stringFormat()
                }
            }
        }
    }
    
    /// Method to make phone calls
    /// - Parameter phoneNumber: Phone number to which call have to be made
    func makePhoneCall(phoneNumber: String) {
        guard !phoneNumber.isEmpty else { return }
        let phone = "tel://"
        let phoneNumberformatted = phone + phoneNumber
        guard let url = URL(string: phoneNumberformatted) else { return }
        UIApplication.shared.open(url)
    }
}
