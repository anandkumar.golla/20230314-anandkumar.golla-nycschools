// Display your spinner by calling Loading.start() and hide it with Loading.stop()
// Include asset 'spinner.png'

import UIKit

class Loading {
	static let backgroundColor = UIColor(red: 186.0/255.0, green: 186.0/255.0, blue: 189.0/255.0, alpha: 0.44)
	static let fade = 0.9

	static var indicator: UIView = {
		var view = UIView()
        let screen: CGRect = UIScreen.main.bounds
		var side = screen.width / 4
		var x = (screen.width / 2) - (side / 2)
		var y = (screen.height / 2) - (side / 2)
        view.frame = CGRect(x: 0, y: 0, width:UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
		view.backgroundColor = backgroundColor
		view.layer.cornerRadius = 10
		view.alpha = 0.0
		view.tag = 1
		let image = UIImage(named: "loader.png")
		let imageView = UIImageView(image: image!)
		imageView.frame = CGRect(x: side / 2, y: side, width: side / 2, height: side / 2)
        imageView.center =  view.center
        
		view.addSubview(imageView)
		return view
	}()

	static var animation: CABasicAnimation = {
		let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
		rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(.pi * 2.0)
		rotateAnimation.duration = 2.0
		rotateAnimation.repeatCount = Float.infinity
		return rotateAnimation
	}()

	static func start () {
        if let window :UIWindow = UIApplication.shared.keyWindow {
			var found: Bool = false
			for subview in window.subviews {
				if subview.tag == 1 {
					found = true
				}
			}
			if !found {
				for subview in indicator.subviews {
                    subview.layer.add(animation, forKey: nil)
				}
				window.addSubview(indicator)
                UIView.animate(withDuration: fade, animations: {
					self.indicator.alpha = 1.0
				})
			}
		}
	}

	static func stop () {
        UIView.animate(withDuration: fade, animations: {
		}, completion: { (value: Bool) in
            DispatchQueue.main.async {
                //Do UI Code here.
                //Call Google maps methods.
                self.indicator.alpha = 0.0
                self.indicator.removeFromSuperview()
                for subview in self.indicator.subviews {
                    subview.layer.removeAllAnimations()
                }
            }
		})
	}
}
