//
//  Constants.swift
//  20230314-AnandKumarGolla-NYCSchools
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import Foundation


enum NavTitle: String {
    case Schools = "Schools"
    case SchoolDetails = "School Details"
}
