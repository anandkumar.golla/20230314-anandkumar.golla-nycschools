//
//  School.swift
//  20230314-AnandKumarGolla-NYCSchools
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import Foundation
import UIKit

struct School: Codable {
    let dbn: String
    let school_name: String
    let overview_paragraph: String?
    let phone_number: String?
    let school_email: String?
    let website: String?
    let school_sports: String?
    let location: String
}

extension School {
    var address: String {
        String(location.split(separator: "(").first ?? "")
    }
}
