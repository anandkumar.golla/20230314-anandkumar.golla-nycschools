//
//  SchoolsView.swift
//  20230314-AnandKumarGolla-NYCSchools
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import UIKit
import SwiftUI

class SchoolsView: UIViewController {
    @IBOutlet weak var schoolsTableView: UITableView!
    @IBOutlet weak var searchNameOrAddress: UISearchBar!
    @IBOutlet weak var schoolsView: UIView!
    @IBOutlet weak var noSchoolsView: UIView!
    
    let refreshControl = UIRefreshControl()
    
    let service = SchoolsService()

    lazy var viewModel : SchoolsViewModel = {
        let viewModel = SchoolsViewModel(schoolsViewService: service)
        return viewModel
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callBacks()
        handleInitialUI()
        fetchSchools()
    }
    
    /// Method that handled inital UI
    func handleInitialUI() {
        searchNameOrAddress.delegate = self
        self.setupToHideKeyboardOnTapOnView()
        self.title = NavTitle.Schools.rawValue
        
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Schools")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        schoolsTableView.addSubview(refreshControl)
    }
    
    /// Method that is called on pull to refresh
    /// - Parameter sender: sender from pull to refresh
    @objc func refresh(_ sender: AnyObject) {
        fetchSchools()
    }
    
    /// Method to refresh UI after schools webservice call
    func refreshUIAfterService() {
        DispatchQueue.main.async { [weak self] in
            self?.noSchoolsView.isHidden = true
            self?.schoolsView.isHidden = false
            self?.schoolsTableView.reloadData()
        }
    }
    
    /// Method that defined all the call backs from the viewModel
    func callBacks() {
        viewModel.onErrorHandling = { [weak self] error in
            Loading.stop()
            self?.refreshControl.endRefreshing()
            self?.refreshUIAfterService()
            self?.showAlert(title: "An error occured", message: error?.stringFormat() ?? "")
        }
        viewModel.onFetchSchools = { [weak self] schools in
            Loading.stop()
            self?.refreshControl.endRefreshing()
            if self?.viewModel.schools.count ?? 0 > 0 {
                self?.refreshUIAfterService()
            } else {
                self?.showAlert(message: "No Schools found")
            }
        }
    }
    
    ///  Method to fetch list of schools
    func fetchSchools() {
        Loading.start()
        viewModel.fetchSchools()
    }
    
    /// Method to open School Details screen( swiftUI)
    /// - Parameter selectedSchool: School object that needs to be displayed on the detail screen
    func openSwiftUIScreen(selectedSchool: School) {
        let swiftUIViewController = UIHostingController(rootView: SchoolDetailsView(school: selectedSchool))
        self.navigationController?.pushViewController(swiftUIViewController, animated: true)
    }
}

extension SchoolsView: UITableViewDataSource, UITableViewDelegate {
    
    /// Table view data source methos
    /// - Parameters:
    ///   - tableView: tableview
    ///   - section: current section
    /// - Returns: number of rows needed for section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.searching {
            return viewModel.searchedSchools.count
        } else {
            return viewModel.schools.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolsCell") as! SchoolsCell
        let school = viewModel.searching ? viewModel.searchedSchools[indexPath.row] : viewModel.schools[indexPath.row]
        cell.schoolName.text = school.school_name
        cell.address.text = school.address
        cell.selectionStyle = .none
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = viewModel.searching ? viewModel.searchedSchools[indexPath.row] : viewModel.schools[indexPath.row]
        openSwiftUIScreen(selectedSchool: school)
    }
    
}

extension SchoolsView: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            viewModel.searching = false
            noSchoolsView.isHidden = true
            schoolsView.isHidden = false
            schoolsTableView.reloadData()
        } else {
            viewModel.searchedSchools = viewModel.schools.filter({
                $0.school_name.lowercased().contains(searchText.lowercased()) ||
                $0.location.lowercased().contains(searchText.lowercased())
            })
            viewModel.searching = true
            if viewModel.searchedSchools.count == 0 {
                noSchoolsView.isHidden = false
                schoolsView.isHidden = true
            } else {
                noSchoolsView.isHidden = true
                schoolsView.isHidden = false
                schoolsTableView.reloadData()
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.searching = false
        searchBar.text = ""
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
}
