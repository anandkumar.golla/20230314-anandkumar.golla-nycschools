//
//  SchoolDetailsView.swift
//  20230314-AnandKumarGolla-NYCSchools
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import SwiftUI

struct SchoolDetailsView: View {
    @StateObject fileprivate var viewModel = SchoolDetailViewModel(service: SATScoresService.shared)
    
    let school: School?

    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 10) {
                headerView
                Divider()
                satScoreView
                Divider()
                overviewView
                Divider()
                sportsView
            }
        }
        .padding(.horizontal)
        .navigationTitle(NavTitle.SchoolDetails.rawValue)
        .onLoad {
            guard let dbn = school?.dbn else { return }
            viewModel.fetchSatScore(dbn: dbn)
        }
        .alert(viewModel.errorString, isPresented: $viewModel.showingAlert) {
            Button("OK", role: .cancel) { }
        }
    }
    
    /// View to display header of the screen like School name, address, phone number, email and websight
    var headerView: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text(school?.school_name ?? "")
                .font(.body.bold())
            Text(school?.address ?? "")
                .font(.callout)
            Text(school?.phone_number ?? "")
                .font(.callout)
                .foregroundColor(.blue)
                .onTapGesture {
                    viewModel.makePhoneCall(phoneNumber: school?.phone_number ?? "")
                }
            Text(school?.school_email ?? "")
                .font(.callout)
                .foregroundColor(.blue)
                .onTapGesture {
                    EmailHelper.shared.sendEmail(subject: "Anything...", body: "", to: school?.school_email ?? "")
                }

            Link(school?.website ?? "", destination: URL(string: "http://\(school?.website ?? "")")!)

        }
    }
    
    /// View that display overview of the school
    var overviewView: some View {
        VStack(alignment: .leading) {
            Text("Overview")
                .font(.body.bold())
            Text(school?.overview_paragraph ?? "NA")
                .font(.callout)
            HStack {
                
            }
        }
    }
    
    /// View that display sports details
    var sportsView: some View {
        VStack(alignment: .leading) {
            Text("Sports")
                .font(.body.bold())
            Text(school?.school_sports ?? "NA")
                .font(.callout)
            HStack {
                
            }
        }
    }
    
    /// View that display sat scores of the school
    @ViewBuilder var satScoreView: some View {
        if let score = viewModel.satScore {
            VStack(alignment: .leading) {
                HStack {
                    Text("Num of sat test takers: ")
                        .font(.body.bold())
                    Text(score.num_of_sat_test_takers ?? "")
                        .font(.callout)
                }
                HStack {
                    Text("Sat critical reading avg score: ")
                        .font(.body.bold())
                    Text(score.sat_critical_reading_avg_score ?? "")
                        .font(.callout)
                }
                HStack {
                    Text("Sat math avg score: ")
                        .font(.body.bold())
                    Text(score.sat_math_avg_score ?? "")
                        .font(.callout)
                }
                HStack {
                    Text("Sat writing avg score: ")
                        .font(.body.bold())
                    Text(score.sat_writing_avg_score ?? "")
                        .font(.callout)
                }
            }
        } else {
            VStack {
                    ProgressView()
                }
        }
    }
}

struct SchoolDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailsView(school: School(dbn: "dbn", school_name: "School Name", overview_paragraph: "Description", phone_number: "1234", school_email: "a@g.com", website: "http://testing.com", school_sports: nil, location: "Address"))
    }
}
