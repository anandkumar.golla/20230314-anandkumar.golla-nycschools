//
//  SchoolsService.swift
//  20230314-AnandKumarGolla-NYCSchools
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import Foundation

protocol SchoolsServiceProtocol : AnyObject {
    func fetchSchools(_ completion: @escaping ((Result<[School], ErrorResult>) -> Void))
}

class SchoolsService: SchoolsServiceProtocol {
    
    static let shared = SchoolsService()
    var task : URLSessionTask?
    
    /// Method to fetch list of schools from the server
    /// - Parameter completion: Completion handler that contails list of schools on success and error on failure
    func fetchSchools(_ completion: @escaping ((Result<[School], ErrorResult>) -> Void)) {
        
        // cancel previous request if already in progress
        self.cancelFetchSchools()
        
        task = RequestService().loadData(urlString: APPURL.SchoolsUrl, completion: { dataResult in
            DispatchQueue.global(qos: .background).async(execute: {
                switch dataResult {
                case .success(let data) :
                    SchoolsService.parse(data: data, completion: completion)
                    break
                case .failure(let error) :
                    print("Network error \(error)")
                    completion(.failure(.network(string: "Network error " + error.localizedDescription)))
                    break
                }
            })
        })
    }
    
    /// Method to parse schools array
    /// - Parameters:
    ///   - data: data that we got from server
    ///   - completion: Completion handler that contails list of schools on success and error on failure
    static func parse(data: Data, completion : (Result<[School], ErrorResult>) -> Void) {
        do {
            let finalResult = try JSONDecoder().decode([School].self, from: data)
            completion(.success(finalResult))
        } catch {
            // can't parse json
            completion(.failure(.parser(string: "Error while parsing json data")))
        }
    }
    
    /// Method to cancel the ongoing service
    func cancelFetchSchools() {
        
        if let task = task {
            task.cancel()
        }
        task = nil
    }
    
}
