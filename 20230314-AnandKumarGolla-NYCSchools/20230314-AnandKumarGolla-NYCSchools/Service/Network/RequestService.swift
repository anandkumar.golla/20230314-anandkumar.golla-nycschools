//
//  RequestService.swift
//  20230314-AnandKumarGolla-NYCSchools
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import Foundation

final class RequestService {
    
    /// Method to make service call to server
    /// - Parameters:
    ///   - urlString: url to which service have to be made
    ///   - method: Type of the service, defaulted to GET
    ///   - body: Body of the request
    ///   - session: URLSession to make service calls
    ///   - completion: completion handler to return data on success and error on failure
    /// - Returns: returns Task
    func loadData(urlString: String, method:RequestFactory.Method = .GET, session: URLSession = URLSession(configuration: .default), completion: @escaping (Result<Data, ErrorResult>) -> Void) -> URLSessionTask? {
        return loadData(urlString: urlString, body: nil) { result in
            completion(result)
        }
    }
    
    /// Method to make service call to server
    /// - Parameters:
    ///   - urlString: url to which service have to be made
    ///   - method: Type of the service, defaulted to GET
    ///   - body: Body of the request
    ///   - session: URLSession to make service calls
    ///   - completion: completion handler to return data on success and error on failure
    /// - Returns: returns Task
    func loadData(urlString: String, method:RequestFactory.Method = .GET, body: Data?, session: URLSession = URLSession(configuration: .default), completion: @escaping (Result<Data, ErrorResult>) -> Void) -> URLSessionTask? {
        
        guard let url = URL(string: urlString) else {
            completion(.failure(.network(string: "Wrong url format")))
            return nil
        }
        
        let request = RequestFactory.request(method: method, url: url, body: body)
        
        if let reachability = Reachability(), !reachability.isReachable {
//            request.cachePolicy = .returnCacheDataDontLoad
            completion(.failure(.network(string: "Please connect to network")))
            return nil
        }
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(.failure(.network(string: "An error occured during request :" + error.localizedDescription)))
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse, 200...299 ~= httpResponse.statusCode else {
                completion(.failure(.network(string: "An error occured during request :")))
                return
            }
            if let data = data {
                completion(.success(data))
            }
        }
        task.resume()
        return task
    }
}
