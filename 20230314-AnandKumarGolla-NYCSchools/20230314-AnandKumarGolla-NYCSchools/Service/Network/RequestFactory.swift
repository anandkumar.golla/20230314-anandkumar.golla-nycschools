//
//  RequestFactory.swift
//  20230314-AnandKumarGolla-NYCSchools
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import Foundation

final class RequestFactory {
    enum Method: String {
        case GET
        case POST
        case PUT
        case DELETE
        case PATCH
    }
    
    /// Method to prepare request
    /// - Parameters:
    ///   - method: Type of the request i.e., GET/POST/PUT/DELETE/PATCH
    ///   - url: Url to which service have to make
    ///   - body: body of the request and this is optional
    /// - Returns: Prepares request and returns it
    static func request(method: Method, url: URL, body: Data?) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        if let data = body {
            request.httpBody = data
        }
        return request
    }
}
