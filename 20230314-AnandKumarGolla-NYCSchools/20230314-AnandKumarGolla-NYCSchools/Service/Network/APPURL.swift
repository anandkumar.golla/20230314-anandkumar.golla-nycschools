//
//  APPURL.swift
//  20230314-AnandKumarGolla-NYCSchools
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import Foundation

struct APPURL {

    private struct Domains {
        static let Prod = "https://data.cityofnewyork.us/resource"
        static let Dev = "https://data.cityofnewyork.us/resource"
        static let Mocking = "https://data.cityofnewyork.us/resource"
        static let UAT = "https://data.cityofnewyork.us/resource"
        static let QA = "https://data.cityofnewyork.us/resource"
    }
    
    private  static let BaseURL = Domains.Prod
    
    static var SchoolsUrl: String {
        return BaseURL  + "/s3k6-pzi2.json"
    }
    
    static var SATScoreUrl: String {
        return BaseURL  + "/f9bf-2cp4.json"
    }
}
