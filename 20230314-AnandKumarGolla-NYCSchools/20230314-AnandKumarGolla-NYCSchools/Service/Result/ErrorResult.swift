//
//  ErrorResult.swift
//  20230314-AnandKumarGolla-NYCSchools
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import Foundation

enum ErrorResult: Error {
    case network(string: String)
    case parser(string: String)
    case custom(string: String)
    
    func stringFormat() -> String {
        switch self {
        case .network(let string):
            return string
        case .parser(let string):
            return string
        case .custom(let string):
            return string
        }
    }
}
