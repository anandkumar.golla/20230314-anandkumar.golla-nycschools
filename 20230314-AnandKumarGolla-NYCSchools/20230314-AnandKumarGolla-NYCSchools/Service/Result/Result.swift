//
//  Result.swift
//  20230314-AnandKumarGolla-NYCSchools
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import Foundation

enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}
