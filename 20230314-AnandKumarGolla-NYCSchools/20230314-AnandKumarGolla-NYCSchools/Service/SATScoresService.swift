//
//  SATScoresService.swift
//  20230314-AnandKumarGolla-NYCSchools
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import Foundation

protocol SATScoresServiceProtocol : AnyObject {
    func fetchSatScores(_ dbn : String, completion: @escaping ((Result<SatScore?, ErrorResult>) -> Void))
}

class SATScoresService: SATScoresServiceProtocol {
    
    static let shared = SATScoresService()
    var task : URLSessionTask?
    
    /// Method to fetch list of Sat Scores from the server
    /// - Parameters:
    ///   - dbn: zId of the school
    ///   - completion: Completion handler that contails SatScore on success and error on failure
    func fetchSatScores(_ dbn : String, completion: @escaping ((Result<SatScore?, ErrorResult>) -> Void)) {
        
        // cancel previous request if already in progress
        self.cancelFetchSATScores()
        let url = APPURL.SATScoreUrl + "?dbn=\(dbn)"
        task = RequestService().loadData(urlString: url, completion: { dataResult in
            DispatchQueue.global(qos: .background).async(execute: {
                switch dataResult {
                case .success(let data) :
                    SATScoresService.parse(data: data, completion: completion)
                    break
                case .failure(let error) :
                    print("Network error \(error)")
                    completion(.failure(.network(string: "Network error " + error.localizedDescription)))
                    break
                }
            })
        })
    }
    
    /// Method to parse SatScore array
    /// - Parameters:
    ///   - data: data that we got from server
    ///   - completion: Completion handler that contails SatScore on success and error on failure
    static func parse(data: Data, completion : (Result<SatScore?, ErrorResult>) -> Void) {
        do {
            let finalResult = try JSONDecoder().decode([SatScore].self, from: data)
            completion(.success(finalResult.first))
        } catch {
            // can't parse json
            completion(.failure(.parser(string: "Error while parsing json data")))
        }
    }
    
    /// Method to cancel the ongoing service
    func cancelFetchSATScores() {
        
        if let task = task {
            task.cancel()
        }
        task = nil
    }
    
}
