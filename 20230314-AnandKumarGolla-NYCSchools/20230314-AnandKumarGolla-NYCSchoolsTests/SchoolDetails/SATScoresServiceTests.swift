//
//  SATScoresServiceTests.swift
//  20230314-AnandKumarGolla-NYCSchoolsTests
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import XCTest
@testable import _0230314_AnandKumarGolla_NYCSchools

final class SATScoresServiceTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_SATScoresService_cancelFetchSchools() {
        
        SATScoresService.shared.fetchSatScores("") { _ in
            // ignore call
        }
        // Expected to task nil after cancel
        SATScoresService.shared.cancelFetchSATScores()
        XCTAssertNil(SATScoresService.shared.task, "Expected task nil")
    }
    
    func test_SchoolsService_fetchSatScores() {
        
        let expectation = XCTestExpectation(description: "Fetch SATScores")

        SATScoresService.shared.fetchSatScores("11X253") { result in
            switch result {
            case .success(let schools) :
                XCTAssertNotNil(schools)
                expectation.fulfill()
            case .failure(let error) :
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5.0)
    }

}
