//
//  SchoolDetailViewModelTests.swift
//  20230314-AnandKumarGolla-NYCSchoolsTests
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import XCTest
@testable import _0230314_AnandKumarGolla_NYCSchools

final class SchoolDetailViewModelTests: XCTestCase {
    
    fileprivate var mockServiceSucess : MockSATScoresServiceSucess!
    var sucessViewModel : SchoolDetailViewModel!
    
    fileprivate var mockServiceFailure : MockSATScoresServiceFailure!
    var failureViewModel : SchoolDetailViewModel!

    var noServiceViewModel : SchoolDetailViewModel!

    override func setUpWithError() throws {
        self.mockServiceSucess = MockSATScoresServiceSucess()
        self.sucessViewModel = SchoolDetailViewModel(service: mockServiceSucess)
        
        self.mockServiceFailure = MockSATScoresServiceFailure()
        self.failureViewModel = SchoolDetailViewModel(service: mockServiceFailure)
        
        self.noServiceViewModel = SchoolDetailViewModel(service: nil)
    }

    override func tearDownWithError() throws {
        self.mockServiceSucess = nil
        self.sucessViewModel = nil
        self.mockServiceFailure = nil
        self.failureViewModel = nil
        self.noServiceViewModel = nil
        super.tearDown()
    }
    
    func test_SchoolsViewModel_fetchSchools_failure() {
        let expectation = XCTestExpectation(description: "fetchSatScore Failure")
        
        failureViewModel.$errorString.sink { _ in
        } receiveValue: { result in
            XCTAssertNotNil(result)
            expectation.fulfill()
        }
        
        failureViewModel.fetchSatScore(dbn: "11X253")

        wait(for: [expectation], timeout: 5.0)
    }

}

fileprivate class MockSATScoresServiceSucess : SATScoresServiceProtocol {
    func fetchSatScores(_ dbn: String, completion: @escaping ((_0230314_AnandKumarGolla_NYCSchools.Result<_0230314_AnandKumarGolla_NYCSchools.SatScore?, _0230314_AnandKumarGolla_NYCSchools.ErrorResult>) -> Void)) {
        completion(.success(SatScore(dbn: "123", school_name: "Test School", num_of_sat_test_takers: "100", sat_critical_reading_avg_score: "100", sat_math_avg_score: "100", sat_writing_avg_score: "100")))

    }
    
    
}

fileprivate class MockSATScoresServiceFailure : SATScoresServiceProtocol {
    func fetchSatScores(_ dbn: String, completion: @escaping ((_0230314_AnandKumarGolla_NYCSchools.Result<_0230314_AnandKumarGolla_NYCSchools.SatScore?, _0230314_AnandKumarGolla_NYCSchools.ErrorResult>) -> Void)) {
        completion(.failure(.parser(string: "Error while parsing json data")))
    }
}
