//
//  SchoolsServiceTests.swift
//  20230314-AnandKumarGolla-NYCSchoolsTests
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import XCTest
@testable import _0230314_AnandKumarGolla_NYCSchools

final class SchoolsServiceTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_SchoolsService_cancelFetchSchools() {
        
        SchoolsService.shared.fetchSchools { _ in
            // ignore call
        }
        // Expected to task nil after cancel
        SchoolsService.shared.cancelFetchSchools()
        XCTAssertNil(SchoolsService.shared.task, "Expected task nil")
    }
    
    func test_SchoolsService_fetchSchools() {
        
        let expectation = XCTestExpectation(description: "Fetch Schools")

        SchoolsService.shared.fetchSchools { result in
            switch result {
            case .success(let schools) :
                XCTAssertNotNil(schools)
                XCTAssertGreaterThan(schools.count, 0)
                expectation.fulfill()
            case .failure(let error) :
                XCTAssertNotNil(error)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5.0)
    }

}
