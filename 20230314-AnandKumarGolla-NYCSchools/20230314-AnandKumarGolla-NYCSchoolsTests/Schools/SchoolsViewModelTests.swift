//
//  SchoolsBiewModelTests.swift
//  20230314-AnandKumarGolla-NYCSchoolsTests
//
//  Created by Anand Kumar Golla on 14/03/23.
//

import XCTest
@testable import _0230314_AnandKumarGolla_NYCSchools

final class SchoolsViewModelTests: XCTestCase {
    
    fileprivate var mockSchoolsServiceSucess : MockSchoolsServiceSucess!
    var sucessViewModel : SchoolsViewModel!
    
    fileprivate var mockSchoolsServiceFailure : MockSchoolsServiceFailure!
    var failureViewModel : SchoolsViewModel!

    var noServiceViewModel : SchoolsViewModel!

    override func setUpWithError() throws {
        self.mockSchoolsServiceSucess = MockSchoolsServiceSucess()
        self.sucessViewModel = SchoolsViewModel(schoolsViewService: mockSchoolsServiceSucess)
        
        self.mockSchoolsServiceFailure = MockSchoolsServiceFailure()
        self.failureViewModel = SchoolsViewModel(schoolsViewService: mockSchoolsServiceFailure)
        
        self.noServiceViewModel = SchoolsViewModel(schoolsViewService: nil)
    }

    override func tearDownWithError() throws {
        self.mockSchoolsServiceSucess = nil
        self.sucessViewModel = nil
        self.mockSchoolsServiceFailure = nil
        self.failureViewModel = nil
        self.noServiceViewModel = nil
        super.tearDown()
    }

    func test_SchoolsViewModel_fetchSchools_noService() {
        let expectation = XCTestExpectation(description: "Fetch Failure No Service")
        
        noServiceViewModel.onErrorHandling = { error in
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.stringFormat(), "Missing service")
            expectation.fulfill()
        }
        
        noServiceViewModel.fetchSchools()
        wait(for: [expectation], timeout: 5.0)
    }

    func test_SchoolsViewModel_fetchSchools_sucess() {
        let expectation = XCTestExpectation(description: "Fetch Sucess Schools")
        
        sucessViewModel.onFetchSchools = { [weak self] status in
            XCTAssertNotNil(status)
            XCTAssertTrue(status)
            XCTAssertEqual(self?.sucessViewModel.schools.count, 1)
            XCTAssertEqual(self?.sucessViewModel.schools.first?.dbn, "123")
            XCTAssertEqual(self?.sucessViewModel.schools.first?.school_name, "Test School")
            XCTAssertNil(self?.sucessViewModel.schools.first?.overview_paragraph)
            XCTAssertNil(self?.sucessViewModel.schools.first?.phone_number)
            XCTAssertNil(self?.sucessViewModel.schools.first?.school_email)
            XCTAssertNil(self?.sucessViewModel.schools.first?.website)
            XCTAssertNil(self?.sucessViewModel.schools.first?.school_sports)
            XCTAssertNotNil(self?.sucessViewModel.schools.first?.location)
            XCTAssertEqual(self?.sucessViewModel.schools.first?.location, "NYC")

            expectation.fulfill()
        }
        
        sucessViewModel.fetchSchools()
        wait(for: [expectation], timeout: 5.0)
    }
    
    func test_SchoolsViewModel_fetchSchools_failure() {
        let expectation = XCTestExpectation(description: "Fetch Failure Schools")
        
        failureViewModel.onErrorHandling = { error in
            XCTAssertNotNil(error)
            XCTAssertEqual(error?.stringFormat(), "Error while parsing json data")
            expectation.fulfill()
        }
        
        failureViewModel.fetchSchools()
        wait(for: [expectation], timeout: 5.0)
    }
}

fileprivate class MockSchoolsServiceSucess : SchoolsServiceProtocol {
    func fetchSchools(_ completion: @escaping ((_0230314_AnandKumarGolla_NYCSchools.Result<[_0230314_AnandKumarGolla_NYCSchools.School], _0230314_AnandKumarGolla_NYCSchools.ErrorResult>) -> Void)) {
        completion(.success([School(dbn: "123", school_name: "Test School", overview_paragraph: nil, phone_number: nil, school_email: nil, website: nil, school_sports: nil, location: "NYC")]))

    }
}

fileprivate class MockSchoolsServiceFailure : SchoolsServiceProtocol {
    func fetchSchools(_ completion: @escaping ((_0230314_AnandKumarGolla_NYCSchools.Result<[_0230314_AnandKumarGolla_NYCSchools.School], _0230314_AnandKumarGolla_NYCSchools.ErrorResult>) -> Void)) {
        completion(.failure(.parser(string: "Error while parsing json data")))
    }
}
